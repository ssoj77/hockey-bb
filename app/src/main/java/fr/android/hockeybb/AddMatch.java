package fr.android.hockeybb;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static android.app.PendingIntent.getActivity;

public class AddMatch extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private ProgressDialog pDialog;
    private JSONParser jParser = new JSONParser();
    private static final String strURL = "http://taekwondo.sytes.net/hockey_bb/script/select_equipe.php";
    private static final String sendURL = "http://taekwondo.sytes.net/hockey_bb/script/add_match.php";
    private List<String> equipe;
    private Spinner aTeam;
    private EditText bTeam;
    private TextView date;
    private TextView heure;
    private EditText lieu;
    private String equipeString;
    private String adversaire;
    private String dateString;
    private String lieuString;

    private static final String TAG_TABLE = "equipe";
    private static final String TAG_TYPE = "nom";

    @Override
    protected void onCreate(Bundle savedInstanceSate) {
        super.onCreate(savedInstanceSate);
        setContentView(R.layout.add_match);

        aTeam = findViewById(R.id.choixEquipe);
        bTeam = findViewById(R.id.choixAdversaire);
        lieu = findViewById(R.id.lieu);
        Button valider = findViewById(R.id.AMValider);
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                equipeString = aTeam.getSelectedItem().toString();
                adversaire = bTeam.getText().toString();
                dateString = date.getText().toString() + " " + heure.getText().toString() + ":00";
                lieuString = lieu.getText().toString();
                Log.i("date", dateString);
                new AddMatch.SendMatch().execute();
            }
        });

        ImageButton calendar = findViewById(R.id.calendarIcon);
        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");
            }
        });

        ImageButton time = findViewById(R.id.time_picker);
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timePicker = new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(), "time picker");
            }
        });

        ImageView btn_fermer = findViewById(R.id.AMclose);
        btn_fermer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        new AddMatch.LoadTeam().execute();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String currentDateString = String.format("%4d-%02d-%02d", year, month+1,dayOfMonth);

        date = findViewById(R.id.choixDateMatch);
        date.setText(currentDateString);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        heure = findViewById(R.id.heure);
        heure.setText(String.format("%02d:%02d", hourOfDay, minute));
    }

    private class LoadTeam extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AddMatch.this);
            pDialog.setMessage("Loading...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting All products from url         *
         */
        protected String doInBackground(String... args) {
            HashMap<String, String> params = new HashMap<String, String>();
            JSONObject json = jParser.makeHttpRequest(strURL, "POST", params);
            equipe = new ArrayList<>();

            // Check your log cat for JSON reponse
            // Checking for SUCCESS TAG
            try {
                JSONArray team = json.getJSONArray(TAG_TABLE);

                for (int j = 0; j < team.length(); j++) {
                    JSONObject c = team.getJSONObject(j);
                    String type = c.getString(TAG_TYPE);
                    equipe.add(type);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e("Lecture bdd", e.toString());
            }
            return null;
        }

        /**
         * After completing
         * <p>
         * background task Dismiss the progress dialog         *
         **/

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            ArrayAdapter adapter = new ArrayAdapter<String>(AddMatch.this,R.layout.spinner_item, equipe) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    View view = super.getDropDownView(position,convertView,parent);
                    TextView tv = (TextView) view;
                    tv.setTextSize(30);
                    if (position%2 == 0)
                        tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    return view;
                }
            };
            String select = getString(R.string.spinner);
            aTeam.setPrompt(select);
            adapter.setDropDownViewResource(R.layout.spinner_item);
            aTeam.setAdapter(adapter);
        }
    }

    private class SendMatch extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AddMatch.this);
            pDialog.setMessage("Sending...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            HashMap<String, String> params = new HashMap<>();
            params.put("aTeam",equipeString);
            params.put("bTeam",adversaire);
            params.put("date",dateString);
            params.put("lieu",lieuString);

            StringBuilder sbParams = new StringBuilder();
            int i = 0;
            for (String key : params.keySet()) {
                try {
                    if (i != 0){
                        sbParams.append("&");
                    }
                    sbParams.append(key).append("=")
                            .append(URLEncoder.encode(params.get(key), "UTF-8"));

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                i++;
            }

            try {
                URL urlObj = new URL(sendURL);

                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();

                conn.setDoOutput(true);

                conn.setRequestMethod("POST");

                OutputStream os = conn.getOutputStream();

                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8));

                String paramsString = sbParams.toString();

                bufferedWriter.write(paramsString);
                Log.i("url",sendURL);
                Log.i("url",paramsString);
                bufferedWriter.flush();
                bufferedWriter.close();
                os.close();
                InputStream IS = conn.getInputStream();
                IS.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }


        @SuppressLint("WrongConstant")
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            finish();
        }

    }
}
