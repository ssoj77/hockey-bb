package fr.android.hockeybb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        Button next_match = findViewById(R.id.next_match);
        Button add_match = findViewById(R.id.add_match);
        Button gest_equ = findViewById(R.id.set_team);
        Button stat = findViewById(R.id.stat);
        Button cur_match = findViewById(R.id.current_match);


        cur_match.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Bouton match actuel", "action enregistrée");
                Intent nmIntent = new Intent(MainActivity.this, CurrentMatch.class);
                startActivity(nmIntent);
            }
        });

        add_match.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Bouton ajout match", "action enregistrée");
                Intent amIntent = new Intent(MainActivity.this, AddMatch.class);
                startActivity(amIntent);
            }
        });

        gest_equ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Bouton gestion equipe", "action enregistrée");
                Intent geIntent = new Intent(MainActivity.this, GestEqu.class);
                startActivity(geIntent);
            }
        });

        stat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("Bouton statistiques", "action enregistrée");
                    Intent sIntent = new Intent(MainActivity.this, Stats.class);
                    startActivity(sIntent);
                }
        });
        next_match.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Bouton prochain match", "action enregistrée");
                Intent sIntent = new Intent(MainActivity.this, NextMatch.class);
                startActivity(sIntent);
            }
        });
    }
}
